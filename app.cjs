const express = require('express');
const path = require('path');

const PORT = process.env.PORT || 2000;

const app = express();

app.use(express.static(path.resolve(__dirname)));

const index = path.resolve(__dirname, 'index.html')
const problem1 = path.resolve(__dirname, 'src/public/output/1-matches-per-year.json');
const problem2 = path.resolve(__dirname, 'src/public/output/2-matches-won-per-team-per-year.json');
const problem3 = path.resolve(__dirname, 'src/public/output/3-extra-runs-conceded-per-team-year-2016.json');
const problem4 = path.resolve(__dirname, 'src/public/output/4-top-ten-economical-bowlers-year-2015.json');
const problem5 = path.resolve(__dirname, 'src/public/output/5-number-times-each-team-won-toss-and-match.json');
const problem6 = path.resolve(__dirname, 'src/public/output/6-player-won-highest-number-of-player-of-the-match-awards.json');
const problem7 = path.resolve(__dirname, 'src/public/output/7-strike-rate-of-batsman-each-season.json');
const problem8 = path.resolve(__dirname, 'src/public/output/8-highest-number-times-one-player-dismissed-another-player.json');
const problem9 = path.resolve(__dirname, 'src/public/output/9-bowler-with-best-economy-super-overs.json');


app.get('/', (request, response) => {
    response.sendFile(index);
});

app.get('/problem1', (request, response) => {
    response.sendFile(problem1);
});

app.get('/problem2', (request, response) => {
    response.sendFile(problem2);
});

app.get('/problem3', (request, response) => {
    response.sendFile(problem3);
});

app.get('/problem4', (request, response) => {
    response.sendFile(problem4);
});

app.get('/problem5', (request, response) => {
    response.sendFile(problem5);
});

app.get('/problem6', (request, response) => {
    response.sendFile(problem6);
});

app.get('/problem7', (request, response) => {
    response.sendFile(problem7);
});

app.get('/problem8', (request, response) => {
    response.sendFile(problem8);
});

app.get('/problem9', (request, response) => {
    response.sendFile(problem9);
});

app.listen(PORT, () => {
    console.log(`Running Server ${PORT}`);
});

