const path = require('path');
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');
csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataMatchesArray) => {
        const PlayerOfMatchObject = iplDataMatchesArray.reduce(function (accumulator, currentValue) {
            if (accumulator.hasOwnProperty(currentValue.season)) {
                if (accumulator[currentValue.season].hasOwnProperty(currentValue.player_of_match)) {
                    accumulator[currentValue.season][currentValue.player_of_match] += 1;
                } else {
                    accumulator[currentValue.season][currentValue.player_of_match] = 1;
                }
            } else {
                accumulator[currentValue.season] = {};
                accumulator[currentValue.season][currentValue.player_of_match] = 1
            }
            return accumulator;
        }, {});
        console.log(PlayerOfMatchObject);
        const mostPlayerOfMatch = Object.entries(PlayerOfMatchObject)
            .reduce(function (accumulator, currentValue) {
                const yearWise = Object.entries(currentValue[1])
                    .sort((a, b) => {
                        return b[1] - a[1]
                    })
                    .slice(0, 1);
                accumulator[currentValue[0]] = yearWise[0][0];
                return accumulator;
            }, {});

        FileSystem.writeFile('../public/output/6-player-won-highest-number-of-player-of-the-match-awards.json', JSON.stringify(mostPlayerOfMatch), (error) => {
            if (error) {
                throw error;
            }
        });
    })