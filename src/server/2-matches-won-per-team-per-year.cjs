const path = require("path");
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathMatches = path.join(__dirname, '../data/matches.csv');
const outputFilePath = path.resolve(__dirname, '../public/output/2-matches-won-per-team-per-year.json');

csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataMatchesArray) => {
        const matchesOwnPerTeamPerYear = iplDataMatchesArray.reduce(function (accumulator, currentValue) {
            if (accumulator.hasOwnProperty(currentValue.season)) {
                if (accumulator[currentValue.season].hasOwnProperty(currentValue.winner)) {
                    accumulator[currentValue.season][currentValue.winner] += 1;
                } else {
                    accumulator[currentValue.season][currentValue.winner] = 1;
                }
            } else {
                accumulator[currentValue.season] = {};
            }
            return accumulator;
        }, {});
        FileSystem.writeFileSync(outputFilePath, JSON.stringify(matchesOwnPerTeamPerYear), (error) => {
            if (error) {
                throw error;
            }else{
                console.log("Sucessful");
            }
        });
    });
