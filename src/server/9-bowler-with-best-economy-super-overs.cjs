const path = require('path');
const FileSystem = require("fs");
const csvtojson = require("csvtojson");

const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');

csvtojson()
    .fromFile(csvFilePathDeliveries)
    .then((iplDataDeliveriesArray) => {
        const superOverEconomy = iplDataDeliveriesArray.filter(function (element) {
            return element.is_super_over == '1';
        })
            .reduce(function (accumulator, curruntValue) {
                if (curruntValue.wide_runs == '0' || curruntValue.noball_runs == '0') {
                    if (accumulator.hasOwnProperty(curruntValue.bowler)) {
                        accumulator[curruntValue.bowler][0] += Number(curruntValue.batsman_runs);
                        accumulator[curruntValue.bowler][1] += 1;
                    } else {
                        accumulator[curruntValue.bowler] = [Number(curruntValue.batsman_runs), 1];
                    }
                } else {
                    if (accumulator.hasOwnProperty(curruntValue.bowler)) {
                        accumulator[curruntValue.bowler[0]] += Number(curruntValue.total_runs) + 1;
                    } else {
                        accumulator[curruntValue.bowler] = [Number(curruntValue.total_runs), 0];
                    }
                }
                return accumulator;
            }, {});
        const economyArray = Object.entries(superOverEconomy)
            .map(function (element) {
                element[1] = Math.round(element[1][0] / (element[1][1] / 6));
                return element;
            })
            .sort(function (a, b) {
                const nameA = a[1];
                const nameB = b[1];
                if (nameA < nameB) {
                    return -1;
                } else {
                    return 1;
                }
            });
        const bestEcomonyBowler = economyArray[0][0];
        FileSystem.writeFile('../public/output/9-bowler-with-best-economy-super-overs.json', JSON.stringify(bestEcomonyBowler), (error) => {
            if (error) {
                throw error;
            }
        });
    });
