const path = require("path");
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');
const outputFilepath = path.resolve(__dirname, '../public/output/1-matches-per-year.json');

csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataMatchesArray) => {
        const mathesPerYear = iplDataMatchesArray.reduce(function (accumulator, currentValue) {
            if (accumulator[currentValue.season]) {
                accumulator[currentValue.season] += 1;
            } else {
                accumulator[currentValue.season] = 1;
            }
            return accumulator;
        }, {});

        FileSystem.writeFile(outputFilepath, JSON.stringify(mathesPerYear), (error) => {
            if (error) {
                throw error;
            }else{
                console.log('Hello');
            }
        });
    });
