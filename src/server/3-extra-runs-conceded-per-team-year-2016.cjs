const path = require('path');
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');
const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');

csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataObj) => {
        const matchesId2016 = iplDataObj.filter(function (element) {
            return element.season == 2016;
        })
            .map(function (element) {
                return element.id;
            });
        csvtojson()
            .fromFile(csvFilePathDeliveries)
            .then((iplDataDeliveriesObj) => {
                const extraRunsConcededPerTeamYear2016 = iplDataDeliveriesObj.reduce(function (accumulator, currentValue) {
                    if (currentValue.match_id in matchesId2016) {
                        if (accumulator.hasOwnProperty([currentValue.bowling_team])) {
                            accumulator[currentValue.bowling_team] += Number(currentValue.extra_runs);
                        } else {
                            accumulator[currentValue.bowling_team] = Number(currentValue.extra_runs);
                        }
                    }
                    return accumulator;
                }, {});
                FileSystem.writeFile('../public/output/3-extra-runs-conceded-per-team-year-2016.json', JSON.stringify(extraRunsConcededPerTeamYear2016), (error) => {
                    if (error) {
                        throw error;
                    }
                });
            });
    });
