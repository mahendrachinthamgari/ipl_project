const path = require('path');
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');
const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');

csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataMatchesArray) => {
        const seasonsId = iplDataMatchesArray.reduce(function (accumulator, curruntValue) {
            accumulator[curruntValue.id] = curruntValue.season;
            return accumulator;
        }, {})
        csvtojson()
            .fromFile(csvFilePathDeliveries)
            .then((iplDataDeliveriesArray) => {
                const playerDetailsObject = iplDataDeliveriesArray.reduce(function (accumulator, currentValue) {
                    const season = seasonsId[currentValue.match_id]
                    if (accumulator.hasOwnProperty(currentValue.batsman)) {
                        if (accumulator[currentValue.batsman].hasOwnProperty(season)) {
                            accumulator[currentValue.batsman][season].totalRuns += Number(currentValue.batsman_runs);
                            accumulator[currentValue.batsman][season].totalBalls += 1;
                        } else {
                            accumulator[currentValue.batsman][season] = {};
                            accumulator[currentValue.batsman][season].totalRuns = Number(currentValue.batsman_runs);
                            accumulator[currentValue.batsman][season].totalBalls = 1;
                        }
                    } else {
                        accumulator[currentValue.batsman] = {};
                        accumulator[currentValue.batsman][season] = {};
                        accumulator[currentValue.batsman][season].totalRuns = Number(currentValue.total_runs);
                        accumulator[currentValue.batsman][season].totalBalls = 1;
                    }
                    return accumulator;

                }, {});
                const batsmanStrikeList = {};
                Object.keys(playerDetailsObject).map(function (element) {
                    const strikeRate = {};
                    Object.keys(playerDetailsObject[element]).map(function (element2) {
                        const strikeRateCalculation = (playerDetailsObject[element][element2].totalRuns / playerDetailsObject[element][element2].totalBalls) * 100;
                        strikeRate[element2] = strikeRateCalculation.toFixed(2);
                    });
                    batsmanStrikeList[element] = strikeRate;
                });
                FileSystem.writeFile('../public/output/7-strike-rate-of-batsman-each-season.json', JSON.stringify(batsmanStrikeList), (error) => {
                    if (error) {
                        throw error;
                    }
                });
            });
    });