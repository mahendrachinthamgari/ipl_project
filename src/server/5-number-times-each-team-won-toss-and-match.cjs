const path = require('path');
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');

csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataMatchesArray) => {
        const teamTossWinner = iplDataMatchesArray.filter(function (element) {
            return element.toss_winner == element.winner;
        })
            .reduce(function (accumulator, currentValue) {
                if (accumulator.hasOwnProperty(currentValue.winner)) {
                    accumulator[currentValue.winner] += 1;
                } else {
                    accumulator[currentValue.winner] = 1;
                }
                return accumulator;
            }, {});
            
        FileSystem.writeFile('../public/output/5-number-times-each-team-won-toss-and-match.json', JSON.stringify(teamTossWinner), (error) => {
            if (error) {
                throw error;
            }
        });
    });
