const path = require('path');
const csvtojson = require("csvtojson");
const FileSystem = require("fs");

const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');
const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');

csvtojson()
    .fromFile(csvFilePathMatches)
    .then((iplDataMatchesArray) => {
        const matchesId2015 = iplDataMatchesArray.filter(function (element) {
            return element.season == '2015';
        })
            .map(function (element) {
                return element.id;
            });
        csvtojson()
            .fromFile(csvFilePathDeliveries)
            .then((iplDataDeliveriesArray) => {
                const bowlersRunsBalls2015 = iplDataDeliveriesArray.filter(function (element) {
                    return (matchesId2015.includes(element.match_id));
                })
                    .reduce(function (accumulator, currentValue) {
                        if (currentValue.wide_runs == '0' || currentValue.noball_runs == '0') {
                            if (accumulator.hasOwnProperty(currentValue.bowler)) {
                                accumulator[currentValue.bowler][0] += Number(currentValue.batsman_runs);
                                accumulator[currentValue.bowler][1] += 1;
                            } else {
                                accumulator[currentValue.bowler] = [Number(currentValue.batsman_runs), 1];
                            }
                        } else {
                            if (accumulator.hasOwnProperty(currentValue.bowler)) {
                                accumulator[currentValue.bowler[0]] += Number(currentValue.total_runs) + 1;
                            } else {
                                accumulator[currentValue.bowler] = [Number(currentValue.total_runs), 0];
                            }
                        }
                        return accumulator;
                    }, {});
                const economyArray = Object.entries(bowlersRunsBalls2015)
                    .map(function (element) {
                        element[1] = Math.round(element[1][0] / (element[1][1] / 6));
                        return element;
                    })
                    .sort(function (a, b) {
                        const nameA = a[1];
                        const nameB = b[1];
                        if (nameA < nameB) {
                            return -1;
                        } else {
                            return 1;
                        }
                    });
                const topTenEcomonyBowlers2015 = economyArray.slice(0, 10)
                    .map(function (element) {
                    return element[0];
                });
                FileSystem.writeFile('../public/output/4-top-ten-economical-bowlers-year-2015.json', JSON.stringify(topTenEcomonyBowlers2015), (error) => {
                    if (error) {
                        throw error;
                    }
                });
            });
    });
