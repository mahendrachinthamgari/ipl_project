const csvtojson = require("csvtojson");
const FileSystem = require("fs");
const path = require('path');

const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');

csvtojson()
    .fromFile(csvFilePathDeliveries)
    .then((iplDataDeliveriesArray) => {
        const bowerDismissedBatsman = iplDataDeliveriesArray.reduce(function (accumulator, currentValue) {
            if (currentValue.player_dismissed) {
                if (accumulator.hasOwnProperty(currentValue.bowler)) {
                    if (accumulator[currentValue.bowler].hasOwnProperty(currentValue.batsman)) {
                        accumulator[currentValue.bowler][currentValue.batsman] += 1;
                    } else {
                        accumulator[currentValue.bowler][currentValue.batsman] = 1;
                    }
                } else {
                    accumulator[currentValue.bowler] = {};
                }
            }
            return accumulator;
        }, {});
        const bowerDismissedBatsmanArray = Object.entries(bowerDismissedBatsman);
        const BowlerDismissedBatsman = bowerDismissedBatsmanArray.reduce(function (accumulator, currentValue) {
            const currentValueValueArray = Object.entries(currentValue[1]).sort((a, b) => {
                return b[1] - a[1];
            })
            accumulator[currentValue[0]] = currentValueValueArray[0];
            return accumulator;
        }, {});
        const BowlerDismissedBatsmanSorted = Object.entries(BowlerDismissedBatsman)
            .filter(function (element) {
                return element[1];
            })
            .sort(function (a, b) {
                return b[1][1] - a[1][1];
            });
        const mostBowlerDismissedBatsman = BowlerDismissedBatsmanSorted[0];

        FileSystem.writeFile('../public/output/8-highest-number-times-one-player-dismissed-another-player.json', JSON.stringify(`${mostBowlerDismissedBatsman[0]} dismissed ${mostBowlerDismissedBatsman[1][0]} ${mostBowlerDismissedBatsman[1][1]} times.`), (error) => {
            if (error) {
                throw error;
            }
        });
    });